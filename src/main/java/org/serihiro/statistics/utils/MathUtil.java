package org.serihiro.statistics.utils;

public class MathUtil {
    public static long factorial(long n) {
        if (n > 20) {
            throw new IllegalArgumentException("n must be lower than 21");
        }

        long sum = 1;
        for (long i = 2; i <= n; i++) {
            sum *= i;
        }
        return sum;
    }
}
