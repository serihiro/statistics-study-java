package org.serihiro.statistics.models;

import org.serihiro.statistics.utils.MathUtil;

public class Poisson {
    private double lambda;

    public Poisson(double lambda) {
        this.lambda = lambda;
    }

    public double probability(int k) {
        return Math.pow(lambda, k) * Math.exp(-1 * lambda) * Math.pow(MathUtil.factorial(k), -1);
    }

    public double likelihood(int[] k) {
        double result = 1d;

        for (int i = 0; i < k.length; i++) {
            result *= this.probability(k[i]);
        }

        return result;
    }
}
