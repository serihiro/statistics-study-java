package org.serihiro.statistics.models;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class PoissonTest {
    @Test
    public void likelihood() throws Exception {
        Poisson poisson = new Poisson(3.56);
        int[] k = {2, 2, 4};
        assertEquals(0.006181070313902507, poisson.likelihood(k));
    }

    @Test
    public void testProbability() throws Exception {
        Poisson poisson = new Poisson(3.56);
        assertEquals(0.028438824714184505, poisson.probability(0));
        assertEquals(0.10124221598249684, poisson.probability(1));
        assertEquals(0.18021114444884437, poisson.probability(2));
    }
}
